﻿using System.Web.Http;
using JDMallen.AppName.Data.IRepos;
using JDMallen.AppName.Data.Models;

namespace JDMallen.AppName.Controllers {
    public class SessionsController : MasterApiController {

        /// <summary>
        /// Logs in a user, returns a session object that may be stored 
        /// in a cookie or other session management system.
        /// </summary>
        /// <param name="login">Object with "user" and "pwd" params.</param>
        /// <returns></returns>
        public Session Post([FromBody] Login login) {
            return SessionRepo.LogIn(login.user, login.pwd);
        }
        
        /// <summary>
        /// Logs out a user. You may use the username or email in the body.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string Delete(string user) {
            return SessionRepo.LogOut(user);
        }

        public SessionsController(ISessionRepo sessionRepo)
            : base(sessionRepo) {}
    }
}
