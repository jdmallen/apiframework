﻿using System.Web.Http;
using JDMallen.AppName.Data.IRepos;
using Newtonsoft.Json;

namespace JDMallen.AppName.Controllers
{
    public abstract class MasterApiController : ApiController {
        protected ISessionRepo SessionRepo { get; set; }

        protected MasterApiController(ISessionRepo sessionRepo) {
            SessionRepo = sessionRepo;
        }

        public static string SuperSerialize(object obj) {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
    }
}
