﻿// ReSharper disable InconsistentNaming
namespace JDMallen.AppName.Data.Models {
    public class Login {
        public string user { get; set; }
        public string pwd { get; set; }
    }
}
