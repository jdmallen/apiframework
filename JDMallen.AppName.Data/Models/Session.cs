﻿using System;
using System.Text;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Models {
    public class Session : Ent {
        public Session(string email, string userName, string additionalInfo = null) {
            Email = email;
            Username = userName;
            if (!string.IsNullOrWhiteSpace(additionalInfo))
                AdditionalInfo = additionalInfo;
            CreationDate = DateTime.UtcNow;
            ExpirationDate = CreationDate.AddDays(7.0);
            SessionID =
                Convert.ToBase64String(
                    Encoding.UTF8.GetBytes(email + ":" +
                                           ExpirationDate.ToFileTimeUtc()));
        }

        public Session() {}

        public string SessionID { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
