﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Dapper;
using JDMallen.AppName.Data.Context.Config;
using JDMallen.AppName.Data.Context.Models;
using JDMallen.AppName.Data.IRepos;
using JDMallen.AppName.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Context.Repos {
    public class SessionRepo : Repo<AppContext, Session>,
        ISessionRepo {
        public SessionRepo(AppContext context) : base(context) {}

        public Session GetSessionByID(string id) {
            return
                Map<Session>(
                    Context.MainDBContext.Query<DBSession>(
                        SQLQuery.SQLQuery.ReadSessionByID, new {sessionID = id},
                        commandType: CommandType.Text)).FirstOrDefault();
        }

        public Session LogIn(string usernameOrEmail, string password) {
//            UserRepo userRepo = new UserRepo(Context);
            // from here: http://www.regular-expressions.info/email.html
            Regex regex =
                new Regex(
                    @"\A[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\z");
            bool isEmail = regex.IsMatch(usernameOrEmail);
//            User userLookup = isEmail
//                ? userRepo.GetUserByEmail(usernameOrEmail)
//                : userRepo.GetUserByUserName(usernameOrEmail);

//            if (userLookup == null ||
//                !PasswordHash.ValidatePassword(password, userLookup.PasswordHash)) {
//                return null;
//            }
            // TODO: populate session object with user info
            Session session = new Session();
            Context.MainDBContext.Query(SQLQuery.SQLQuery.CreateSession,
                session.ToParams(), commandType: CommandType.Text);

            return session;
        }

        public string LogOut(string usernameOrEmail) {
            bool returnable = false;
//            UserRepo userRepo = new UserRepo(Context);
            // from here: http://www.regular-expressions.info/email.html
            Regex regex =
                new Regex(
                    @"\A[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\z");
            bool isEmail = regex.IsMatch(usernameOrEmail);
//            User userLookup = isEmail
//                ? userRepo.GetUserByEmail(usernameOrEmail)
//                : userRepo.GetUserByUserName(usernameOrEmail);

//            if (userLookup == null) {
//                return "User not found in database.";
//            }

            List<Session> userSessions = ListSessionsByUser(usernameOrEmail);
            foreach (Session session in userSessions) {
                Context.MainDBContext.Query(SQLQuery.SQLQuery.DeleteSessionByID,
                    new {session.SessionID});
                returnable = true;
            }
            return returnable? "Successfully logged out." : "Not logged in.";
        }

        public bool ValidateSession(string sessionID) {
            Session matchingSession = GetSessionByID(sessionID);
            DateTime now = DateTime.UtcNow;
            return matchingSession.CreationDate.CompareTo(now) < 0 &&
                   matchingSession.ExpirationDate.CompareTo(now) > 0;
        }

        public List<Session> ListSessionsByUser(string usernameOrEmail) {
            return
                Map<Session>(
                    Context.MainDBContext.Query<DBSession>(
                        SQLQuery.SQLQuery.ReadSessionByUserNameOrEmail,
                        new {
                            UserName = usernameOrEmail,
                            Email = usernameOrEmail
                        })).ToList();
        }
    }
}
