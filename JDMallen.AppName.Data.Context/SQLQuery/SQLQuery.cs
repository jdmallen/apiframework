﻿using System.IO;
using System.Reflection;

namespace JDMallen.AppName.Data.Context.SQLQuery {
    internal static class SQLQuery {
        private const string QueryPath =
            @"JDMallen.AppName.Data.Context.SQLQuery.{0}.sql";

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public static string CreateSession { get; private set; }
        public static string CreateUser { get; private set; }
        public static string DeleteSessionByID { get; private set; }
        public static string DeleteUserByID { get; private set; }
        public static string ReadAllUsers { get; private set; }
        public static string ReadSessionByID { get; private set; }
        public static string ReadSessionByUserNameOrEmail { get; private set; }
        public static string ReadUserByEmail { get; private set; }
        public static string ReadUserByID { get; private set; }
        public static string ReadUserByUserName { get; private set; }
        public static string ReadUserInventory { get; private set; }
        public static string SearchSessionByUserNameOrEmail { get; private set; }
        public static string SearchUsers { get; private set; }
        public static string UpdateUser { get; private set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local

        static SQLQuery() {
            LoadQueries();
        }

        private static void LoadQueries() {
            PropertyInfo[] props =
                typeof (SQLQuery).GetProperties(BindingFlags.Public |
                                                 BindingFlags.Static);
            foreach (PropertyInfo prop in props) {
                string query = LoadQuery(prop);
                if (string.IsNullOrEmpty(query)) {
                    continue;
                }
                prop.SetValue(null, query);
            }
        }

        private static string LoadQuery(PropertyInfo property) {
            var assembly = Assembly.GetCallingAssembly();
            var path = string.Format(QueryPath, property.Name);
            var stream = assembly.GetManifestResourceStream(path);

            return stream == null
                ? null
                : new StreamReader(stream).ReadToEnd();
        }
    }
}
