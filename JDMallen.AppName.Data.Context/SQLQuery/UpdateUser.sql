﻿UPDATE [dbo].[Users]
SET [UserName] = @UserName
    ,[PasswordHash] = @PasswordHash
    ,[Email] = @Email
    ,[CreationDate] = @CreationDate
    ,[FirstName] = @FirstName
    ,[LastName] = @LastName
WHERE UserID = @UserID