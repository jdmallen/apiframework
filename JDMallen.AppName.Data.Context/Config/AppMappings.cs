﻿using System.Drawing;
using System.IO;
using AutoMapper;
using JDMallen.AppName.Data.Context.Models;
using JDMallen.AppName.Data.Models;

namespace JDMallen.AppName.Data.Context.Config {
    internal class AppMappings : Profile {
        protected override void Configure() {
            // ReSharper disable UnusedVariable
            IMappingExpression<DBSession, Session> sessionMapping =
                CreateMap<DBSession, Session>();
            // ReSharper restore UnusedVariable
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn) {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
