﻿using System.Data.SqlTypes;

namespace JDMallen.AppName.Data.Context.Models {
    public class DBSession {
        public SqlString SessionID { get; set; }
        public SqlString Email { get; set; }
        public SqlString Username { get; set; }
        public SqlDateTime CreationDate { get; set; }
        public SqlDateTime ExpirationDate { get; set; }
        public SqlString AdditionalInfo { get; set; }
    }
}
